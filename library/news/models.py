from django.db import models
import datetime
from django.utils import timezone
from django.contrib.auth.models import User
class Reportero(models.Model):
    full_name = models.CharField('Nombre Completo', max_length= 100, db_column= 'full_name')

    def __str__(self):
        return self.full_name

class Articulo(models.Model):
    pub_date = models.DateField(default=timezone.now(), db_column= 'pub_date')
    content = models.TextField(db_column = 'content', blank= True, null= True)
    reporter = models.ForeignKey(Reportero, on_delete=models.CASCADE, db_column= 'reporter')

    def __str__(self):
        return self.content

    def was_published_recently(self):
        return datetime.date.today() <= self.pub_date + datetime.timedelta(days=1)

class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')

    def __str__(self):
        return self.question_text

    def was_published_recently(self):
        return datetime.date.today() <= self.pub_date + datetime.timedelta(days=1)


class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    #votes = models.IntegerField(default=0)

    def __str__(self):
        return self.choice_text

class Vote(models.Model):
    choice = models.ForeignKey(Choice, related_name='votes', on_delete=models.CASCADE)
    poll = models.ForeignKey(Question, on_delete=models.CASCADE)
    voted_by = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        unique_together = ("poll", "voted_by")
