from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, JsonResponse
from news.models import Question
import json

def question_api(request):
    question = list(Question.objects.all().values())

    return JsonResponse({'data':question})

def art_details(request, id):
    return HttpResponse('Preguntate por detalles del Articulo {0}'.format(id))

def art_results(request, id):
    return HttpResponse('Preguntaes por resultado del Articulo {0}'.format(id))

def index_question(request):
    question_order = Question.objects.order_by('-pub_date')
    #response = list(map(lambda x: x.question_text, question_order))
    context = {'latest_question_list': question_order}
    print(context)
    return render(request, 'question/index.html',context)
    #return HttpResponse(response)

def details_question(request, id):
    response = get_object_or_404(Question, pk=id)
    return HttpResponse('Question : {0} \n Date: {1}'.format(response.question_text,
                        response.pub_date))
