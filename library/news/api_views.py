from rest_framework.views import APIView
from rest_framework.response import Response
from django.shortcuts import get_object_or_404

from news.models import Question
from news.serializers import *

class QuestionApiViewList(APIView):
    def get(self, request):
        question_all = Question.objects.all()
        print(question_all.values())
        data = QuestionSerializer(question_all, many=True).data
        return Response(data)
