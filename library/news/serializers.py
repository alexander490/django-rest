from rest_framework import serializers
from news.models import Question, Vote, Choice

class QuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question
        fields = '__all__'

class VoteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vote
        fields = '__all__'

class Choice(serializers.ModelSerializer):
    class Meta:
        model = Choice
        fields = '__all__'
