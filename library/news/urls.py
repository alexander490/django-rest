from django.urls import path
from news import views, api_views

urlpatterns = [
    path('art/details/<int:id>', views.art_details),
    path('art/results/<int:id>', views.art_results),
    path('question/index', views.index_question),
    path('question/details/<int:id>', views.details_question),
    path('api/v1/question/', views.question_api),
    path('api/v1/question_rest', api_views.QuestionApiViewList.as_view())

]
