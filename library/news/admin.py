from django.contrib import admin
from news.models import Reportero, Articulo, Choice, Question

class ArticuloAdmin(admin.ModelAdmin):
    list_display = ('content', 'pub_date', 'reporter')
    #exclude = ['pub_date']
    fields = (('pub_date', 'reporter'), 'content')
    readonly_fields = ['pub_date']

class ChoiceAdmin(admin.ModelAdmin):
    list_display= ('choice_text','question','votes')

admin.site.site_header = 'Administración FESEPSA'
admin.site.register(Reportero)
admin.site.register(Articulo, ArticuloAdmin)
admin.site.register(Choice, ChoiceAdmin)
admin.site.register(Question)

# Register your models here.
